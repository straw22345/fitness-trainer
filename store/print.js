export const state = () => ({
  text: ''
})

export const mutations = {
  set (state, text) {
    state.text = text
  }
}

export const actions = {
  async set ({ commit }, text) {
    await new Promise((resolve) => {
      setTimeout(() => {
        commit('set', text)
        resolve()
      }, 500)
    })
  }
}
