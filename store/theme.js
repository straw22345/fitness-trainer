export const state = () => ({
  dark: false,
  mode: 'light-mode'
})

export const mutations = {
  dark (state, value) {
    state.dark = value
    if (value) {
      state.mode = 'dark-mode'
    } else {
      state.mode = 'light-mode'
    }
  }
}
