export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,
  // Going Full Static https://go.nuxtjs.dev/static-target
  target: 'static',
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: process.env.SITE_NAME,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'preload', as: 'style', href: '/css/bootstrap.min.css' },
      { rel: 'preload', as: 'style', href: '/css/bootstrap-dark.min.css' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/scss/index.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '@/plugins/helper.js' },
    { src: '@/plugins/inject.js' },
    { src: '@/plugins/axios.js' },
    { src: '@/plugins/main.js' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    ['@nuxtjs/eslint-module', { fix: true }]
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    ['bootstrap-vue/nuxt', { css: false }],
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
    // https://auth.nuxtjs.org
    '@nuxtjs/auth',
    // https://github.com/rubystarashe/nuxt-vuex-localstorage
    ['nuxt-vuex-localstorage', {
      key: 'foo',
      salt: 'bar',
      localStorage: ['print']
    }],
    ['nuxt-i18n', {
      strategy: 'no_prefix',
      defaultLocale: 'en',
      lazy: true,
      langDir: 'locales/',
      locales: [
        { code: 'en', name: 'English', direction: 'ltr', file: 'en/index.js' }
      ]
    }],
    ['@nuxtjs/recaptcha', {
      hideBadge: false,
      siteKey: process.env.NODE_ENV === 'production' ? process.env.RECAPTCHA_SITE_KEY : process.env.RECAPTCHA_TEST_KEY,
      version: process.env.RECAPTCHA_VERSION,
      size: process.env.RECAPTCHA_SIZE
    }]
  ],

  router: {
    middleware: ['auth']
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.NODE_ENV === 'production' ? process.env.PROD_API_URL : process.env.DEV_API_URL
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
      name: 'KMax Fitness',
      short_name: 'KMax'
    },
    workbox: {
      // for production
      importScripts: ['notification-sw.js'],
      enabled: true
    }
  },

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {},

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/trainer/login', method: 'post', propertyName: 'token' },
          logout: { url: '/trainer/logout', method: 'post' },
          user: { url: '/trainer', method: 'get', propertyName: 'user' }
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend (config, { isDev, isClient }) {
      if (isClient) {
        config.optimization.splitChunks.maxSize = 224000
      }
    }
  },

  /*
  ** Generate configuration
  */
  generate: {
    dir: 'public',
    fallback: true
  },

  // server: {
  //   host: '0.0.0.0',
  //   port: 8000
  // },

  publicRuntimeConfig: {
    SITE_NAME: process.env.SITE_NAME,
    vapidPublicKey: process.env.VAPID_PUBLIC_KEY
  }
}
