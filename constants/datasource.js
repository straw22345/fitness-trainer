const statusColors = [
  { status: 'pending', color: '#808080' },
  { status: 'draft', color: '#808080' },
  { status: 'confirmed', color: '#008000' },
  { status: 'booked', color: '#008000' },
  { status: 'coming', color: '#008000' },
  { status: 'arrived', color: '#008000' },
  { status: 'start_training', color: '#008000' },
  { status: 'expired', color: '#FF0000' },
  { status: 'rejected', color: '#FF0000' },
  { status: 'cancelled', color: '#FF0000' },
  { status: 'completed', color: '#000000' },
  { status: 'finished', color: '#000000' },
  { status: 'finished_training', color: '#000000' },
  { status: 'active', color: '#3c78ef' },
  { status: 'live', color: '#FFA500' },
  { status: 'changed', color: '#FFC107' }
]

export default statusColors
