
const data = [
  {
    id: 'dashboard',
    icon: 'fas fa-tachometer-alt',
    label: 'menu.dashboard',
    module: '*',
    to: '/'
  },
  {
    id: 'calendar',
    label: 'menu.calendar',
    icon: 'fas fa-calendar-alt',
    module: '*',
    to: '/calendar'
  },
  {
    id: 'bookings',
    label: 'menu.bookings',
    icon: 'c-red-500 fas fa-calendar-check',
    module: '*',
    to: '/bookings'
  },
  {
    id: 'live_classes',
    label: 'menu.live_classes',
    icon: 'fas fa-video',
    module: '*',
    to: '/live-classes'
  },
  {
    id: 'nutritions',
    label: 'menu.nutritions',
    icon: 'fas fa-heartbeat',
    module: '*',
    to: '/documents'
  },
  {
    id: 'clients',
    label: 'menu.clients',
    icon: 'fas fa-users',
    module: '*',
    to: '/clients'
  },
  // {
  //   id: 'trainers',
  //   label: 'menu.trainers',
  //   icon: 'fas fa-users',
  //   module: '*',
  //   to: '/trainers'
  // },
  {
    id: 'wallet',
    label: 'menu.wallet',
    icon: 'fas fa-wallet',
    module: '*',
    to: '/wallet'
  },
  // {
  //   id: 'documents',
  //   label: 'menu.documents',
  //   icon: 'fas fa-file-alt',
  //   module: '*',
  //   to: '/documents'
  // },
  // {
  //   id: 'chats',
  //   label: 'menu.chats',
  //   icon: 'fas fa-comments',
  //   module: '*',
  //   to: '/chats'
  // },
  {
    id: 'progress',
    label: 'menu.progress',
    icon: 'fas fa-chart-line',
    module: '*',
    to: '/progress',
    subs: [
      // {
      //   id: 'trainings',
      //   label: 'menu.trainings',
      //   icon: 'fas fa-dumbbell',
      //   to: '/progress/trainings'
      // },
      {
        id: 'weights',
        label: 'menu.bodyweights',
        icon: 'fas fa-weight',
        to: '/progress/weights'
      },
      {
        id: 'measurements',
        label: 'menu.measurements',
        icon: 'fas fa-ruler',
        to: '/progress/measurements'
      },
      {
        id: 'progress_photos',
        label: 'menu.progress_photos',
        icon: 'fas fa-images',
        to: '/progress/photos'
      }
    ]
  }
  // {
  //   id: 'faq',
  //   label: 'menu.faq',
  //   icon: 'fas fa-question-circle',
  //   module: '*',
  //   to: '/faq'
  // }
]

export default data
