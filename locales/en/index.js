import menu from './modules/menu'
import form from './modules/form'

const data = {
  menu,
  form
}

export default data
