(() => {
  'use strict'

  const WebPush = {
    init () {
      self.addEventListener('push', this.notificationPush.bind(this))
      self.addEventListener(
        'notificationclick',
        this.notificationClick.bind(this)
      )
      self.addEventListener(
        'notificationclose',
        this.notificationClose.bind(this)
      )

      // in the service worker
      self.addEventListener('message', (event) => {
        // event is an ExtendableMessageEvent object
        // console.log(`The client sent me a message: ${event.data}`)

        // event.source.postMessage('Hi client')
      })

      self.addEventListener('activate', (event) => {
        // console.log('Activated')
      })

      self.addEventListener('install', (event) => {
        // self.clients.claim()
        // self.skipWaiting() // not for production
        // console.log('Installing!')
      })
    },
    /**
     * Handle notification push event.
     *
     * https://developer.mozilla.org/en-US/docs/Web/Events/push
     *
     * @param {NotificationEvent} event
     */
    notificationPush (event) {
      if (!(self.Notification && self.Notification.permission === 'granted')) {
        // eslint-disable-next-line no-console
        console.log('notification not enabled or permission not granted')
        return
      }

      // https://developer.mozilla.org/en-US/docs/Web/API/PushMessageData
      if (event.data) {
        const data = event.data.json()
        if (data.data.type === 'NOTI_ADD') {
          event.waitUntil(this.sendNotification(event.data.json()))
        }

        // Obtain an array of Window client objects
        const options = {
          includeUncontrolled: true,
          type: 'all'
        }
        self.clients.matchAll(options).then(function (clients) {
          if (clients && clients.length) {
            // // Respond to last focused tab
            // clients[0].postMessage({ type: "MSG_ID" });

            // Respond to all clients
            for (let i = 0; i < clients.length; i++) {
              clients[i].postMessage({ type: data.data.type, noti: data })
            }
          }
        })
      }
    },

    /**
     * Handle notification click event.
     *
     * https://developer.mozilla.org/en-US/docs/Web/Events/notificationclick
     *
     * @param {NotificationEvent} event
     */
    notificationClick (event) {
      const clickedNotification = event.notification
      clickedNotification.close()

      if (event.notification.data.action_url) {
        const promiseChain = self.clients.openWindow(event.notification.data.action_url)// this.showBooking(event.notification)
        event.waitUntil(promiseChain)
      }
    },

    showBooking (notification) {
      // console.log(notification)
      self.clients.openWindow(notification.data.action_url)
    },
    /**
     * Handle notification close event (Chrome 50+, Firefox 55+).
     *
     * https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerGlobalScope/onnotificationclose
     *
     * @param {NotificationEvent} event
     */
    notificationClose (event) {
      self.registration.pushManager.getSubscription().then((subscription) => {
        if (subscription) {
          this.dismissNotification(event, subscription)
        }
      })
    },

    /**
     * Send notification to the user.
     *
     * https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/showNotification
     *
     * @param {PushMessageData|Object} data
     */
    sendNotification (data) {
      return self.registration.showNotification(data.title, data)
    },

    /**
     * Send request to server to dismiss a notification.
     *
     * @param  {NotificationEvent} event
     * @param  {String} subscription.endpoint
     * @return {Response}
     */
    dismissNotification ({ notification }, { endpoint }) {
      if (!notification.data || !notification.data.id) {
        return
      }

      const data = new FormData()
      data.append('endpoint', endpoint)

      // Send a request to the server to mark the notification as read.
      fetch(`/client/notifications/${notification.data.id}/dismiss`, {
        method: 'POST',
        body: data
      })
    }
  }

  WebPush.init()
})()
