export default (context, inject) => {
  const preloader = {
    show () {
      const $el = document.getElementById('preloader')
      if ($el !== null) {
        $el.classList.remove('h-0')
        for (let i = 0; i < $el.children.length; i++) {
          $el.children[i].classList.remove('d-none')
        }
        // $el.children.forEach((element) => {
        //   element.classList.remove('d-none')
        // })
      }
    },
    hide () {
      const $el = document.getElementById('preloader')
      if ($el !== null) {
        $el.classList.add('h-0')
        const timeout = setTimeout(() => {
          for (let i = 0; i < $el.children.length; i++) {
            $el.children[i].classList.add('d-none')
            clearTimeout(timeout)
          }
          // $el.children.forEach((element) => {
          //   element.classList.add('d-none')
          //   clearTimeout(timeout)
          // })
        }, 200)
      }
    }
  }

  inject('preloader', preloader)
}
