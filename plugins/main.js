import '@fortawesome/fontawesome-free/css/all.min.css'
import Vue from 'vue'

import ResizeDetector from 'vue-resize-detector'
import PerfectScrollbar from 'vue-perfect-scrollbar'
import VueSwitch from 'vue-switches'
import Vuelidate from 'vuelidate'
import Snotify, { SnotifyPosition } from 'vue-snotify'
import VueTimeago from 'vue-timeago'
import vueAwesomeCountdown from 'vue-awesome-countdown'
import VueCtkDateTimePicker from 'vue-ctk-date-time-picker'
import VueConfirmDialog from 'vue-confirm-dialog'
import { InPlaceEditorPlugin } from '@syncfusion/ej2-vue-inplace-editor'
import { DropDownListPlugin, MultiSelectPlugin } from '@syncfusion/ej2-vue-dropdowns'
import { SchedulePlugin } from '@syncfusion/ej2-vue-schedule'
import { ChartPlugin } from '@syncfusion/ej2-vue-charts'
import { CalendarPlugin, TimePickerPlugin, DatePickerPlugin } from '@syncfusion/ej2-vue-calendars'

const snotifyOptions = {
  toast: {
    position: SnotifyPosition.rightTop,
    showProgressBar: false,
    timeout: 3000,
    icon: false
  }
}

Vue.use(Vuelidate)
Vue.use(Snotify, snotifyOptions)
Vue.use(VueTimeago, { locale: 'en' })
Vue.use(vueAwesomeCountdown, 'vac')
Vue.use(InPlaceEditorPlugin)
Vue.use(DropDownListPlugin)
Vue.use(MultiSelectPlugin)
Vue.use(SchedulePlugin)
Vue.use(ChartPlugin)
Vue.use(CalendarPlugin)
Vue.use(DatePickerPlugin)
Vue.use(TimePickerPlugin)
Vue.use(VueConfirmDialog)

Vue.component('PerfectScrollbar', PerfectScrollbar)
Vue.component('ResizeDetector', ResizeDetector)
Vue.component('VueSwitch', VueSwitch)
Vue.component('VueCtkDatetimepicker', VueCtkDateTimePicker)
Vue.component('VueConfirmDialog', VueConfirmDialog.default)
