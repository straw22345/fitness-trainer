export default function ({ $axios, redirect }) {
  $axios.onError((err) => {
    const code = parseInt(err.response && err.response.status)
    if (code === 401 && window.$nuxt.$route.name !== 'login') {
      window.$nuxt.$auth.setUser(null)
      redirect('/login')
    } else if (code === 422) {
      let errors = ''
      Object.keys(err.response.data.errors).forEach((key) => {
        errors += '<li><small>' + err.response.data.errors[key] + '</small></li>'
      })
      if (errors.length) {
        window.$nuxt.$snotify.error('', {
          html: `<div class="snotifyToast__body"><b>ERRORS</b></div>
          <div class="snotifyToast__body"><ul>${errors}</ul></div>`
        })
      }
    } else if (typeof (err.response.data.message) === 'string') {
      window.$nuxt.$snotify.error(err.response.data.message)
      if (code === 403) {
        window.$nuxt.$router.back()
      }
    }
  })
}
